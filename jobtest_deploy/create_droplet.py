import os
import digitalocean
from digitalocean import SSHKey

mytoken = os.getenv("DO_API_TOKEN")

manager = digitalocean.Manager(token=mytoken)
keys = manager.get_all_sshkeys()

droplet = digitalocean.Droplet(
    token=mytoken,
    name="jobtest",
    region="nyc1",
    image="ubuntu-18-04-x64",
    size_slug="s-2vcpu-4gb",
    ssh_keys=keys,
    backups=False,
)
droplet.create()

manager = digitalocean.Manager(token=mytoken)
my_droplets = manager.get_all_droplets(droplet.ip_address)
print(my_droplets)
