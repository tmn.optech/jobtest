import os
import digitalocean

manager = digitalocean.Manager(token=os.getenv("DO_API_TOKEN"))
my_droplets = manager.get_all_droplets()
for droplet in my_droplets:
    droplet.destroy()
