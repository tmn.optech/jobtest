# Jobs Test

### CI

Шаги:
* создание vpc через api оператора
* установка docker,docker-compose копирование архива проекта и запуск
* удаление созданных vpc через api оператора

## Детали выполнения

### Создание инфраструктуры в DigitalOcean
```
pip install -r requirements.txt
```
```
export DO_API_TOKEN=xxxxx
```
```
python create_droplet.py
````
### Запуск деплоя роли Ansible на droplets в DO
```
ansible-playbook ./jobtest.yml \
    -i digital_ocean.py \
```
### Примечание
удалить все инстансы в DO
```
python destroy_droplet.py
```

### Конечный результат

nginx проксирует два vhost

* elk.local
* tracks.local

